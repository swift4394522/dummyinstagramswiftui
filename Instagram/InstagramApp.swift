//
//  InstagramApp.swift
//  Instagram
//
//  Created by Aidan Walker on 19/05/2023.
//

import SwiftUI

@main
struct InstagramApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
