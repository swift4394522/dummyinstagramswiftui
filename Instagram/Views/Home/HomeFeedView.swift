//
//  HomeFeedView.swift
//  Instagram
//
//  Created by Aidan Walker on 19/05/2023.
//

import SwiftUI

struct HomeFeedView: View {
    var body: some View {
        NavigationView {
            ScrollView {
                VStack {
                    StoriesView()
//                        .background(Color.red)
                    ForEach(1...5, id: \.self) { num in
                        PostView(userImageName: "user\(num)", imageName: "image\(num)")
                            .padding(.bottom, 20)
                    }
                }
            }
            .navigationTitle("Instagram")
        }
    }
}

struct PostView: View {
    let userImageName: String
    let imageName: String
    
    var body: some View {
        VStack {
            PostHeaderView(userImageName: userImageName)
            .padding()
            
            // Image
            Image(imageName)
                .resizable()
                .frame(width: 400, height: 400, alignment: .center)
                .aspectRatio(contentMode: .fill)
                .background(Color(.secondarySystemBackground))
            HStack {
                // action buttons: like, comment, share
                Button(action: {
                    // Like
                    
                } , label: {
                    Image(systemName: "heart")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 30, height: 30, alignment: .center)
                        .foregroundColor(Color(.label))
                        
                })
                .padding(.trailing, 3)
                Button(action: {
                    // Comment
                    
                } , label: {
                    Image(systemName: "message")
                        .resizable()
                        .frame(width: 30, height: 30, alignment: .center)
                        .aspectRatio(contentMode: .fit)
                        .foregroundColor(Color(.label))
                        
                })
                .padding(.trailing, 3)
                Button(action: {
                    // Download
                    
                } , label: {
                    Image(systemName: "square.and.arrow.up")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 30, height: 30, alignment: .center)
                        .foregroundColor(Color(.label))
                        
                })
                .padding(.trailing, 3)
                Spacer()
            }
            .padding()
            
            HStack {
                // like count
                Image(systemName: "heart.fill")
                    .resizable()
                    .foregroundColor(Color.red)
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 30, height: 30, alignment: .center)
                Text("32 Likes")
                    .font(.system(size: 20))
                    .foregroundColor(Color(.link))
                Spacer()
            }
            .padding()
            .padding(.top, -20)
            
            // Caption
                Text("Oh man I hope 2021 is better than 2020! #newyear #trending #swiftUI")
                    .font(.headline)
                    .padding(.top, -15)
            // Comments
            
            // Date
            HStack {
                Text("1 hour ago")
                    .foregroundColor(Color(.secondaryLabel))
                    .multilineTextAlignment(.leading)
                Spacer()
            }
            .padding(.leading, 15)
        }
    }
}

struct PostHeaderView: View {
    var userImageName: String
    
    var body: some View {
        HStack {
            // user profile picture and username
            Image(userImageName)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 40, height: 40, alignment: .center)
                .cornerRadius(20)
               // .foregroundColor(Color.blue)
            Text("kanyewest")
                .foregroundColor(Color.blue)
                .bold()
            Spacer()
        }
    }
}

struct HomeFeedView_Previews: PreviewProvider {
    static var previews: some View {
        HomeFeedView()
            .previewDevice("iPhone 12 mini")
.previewInterfaceOrientation(.portraitUpsideDown)
    }
}

