//
//  StoriesView.swift
//  Instagram
//
//  Created by Aidan Walker on 19/05/2023.
//

import SwiftUI

struct StoriesView: View {
    let data = (1...5).map({ "user\($0)" })
    
    var body: some View {
        ScrollView(.horizontal) {
            HStack {
                ForEach(data, id: \.self) { imageName in
                    StoryView(imageName: imageName)
                }
//                ForEach(0...10, id: \.self) { num in
//                    StoryView(imageName: "foo")
//                }
//                Image("foo")
//                    .resizable()
//                    .frame(width: 90, height: 90, alignment: .center)
//                    .background(Color.blue)
//                    .cornerRadius(45)
            }
        }
    }
}

struct StoryView: View {
    let imageName: String
    var body: some View {
                Image(imageName)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 90, height: 90, alignment: .center)
                    .background(Color.blue)
                    .cornerRadius(45)
                    .padding(3)
    }
}
