//
//  CaptureView.swift
//  Instagram
//
//  Created by Aidan Walker on 21/05/2023.
//
import Foundation
import SwiftUI

// struct conforms to UIViewControllerRepresentable
struct CaptureView: UIViewControllerRepresentable {
    // what type of view controller do we want to use from SWIFT UI KIT
    // UIImagePickerController - from UI Kit to use camera photo library
    typealias UIViewControllerType = UIImagePickerController

    @Binding var isShown: Bool
    @Binding var image: Image?
    @Binding var isUsingCamera: Bool

    init(isShown: Binding<Bool>,
         image: Binding<Image?>,
         isUsingCamera: Binding<Bool>) {
        _isShown = isShown
        _image = image
        _isUsingCamera = isUsingCamera
    }

    func makeCoordinator() -> Coordinator {
        return Coordinator(isShown: $isShown, image: $image)
    }

    // go ahead and create the ui picker controller the same way you would in a standard UI Kit app and return it to Swift UI
    func makeUIViewController(context: UIViewControllerRepresentableContext<CaptureView>) -> UIImagePickerController {

        // creates a picker
        let picker = UIImagePickerController()
        picker.sourceType = isUsingCamera ? .camera : .photoLibrary
        // convey event from the user, goes ahead and selects an image
        picker.delegate = context.coordinator
        return picker
    }

    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {
        // do nothing
    }
}

// inherits from NSObject and conforms to UINavigationControllerDelegate, UIImagePickerControllerDelegate
class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @Binding var isShown: Bool
    @Binding var image: Image?

    init(isShown: Binding<Bool>,
         image: Binding<Image?>) {
        _isShown = isShown
        _image = image
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // user cancelled
        isShown = false
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        guard let uiimage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        image = Image(uiImage: uiimage)
        isShown = false
    }
}
